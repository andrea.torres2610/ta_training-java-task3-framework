package com.epam.training.andrea_torres.utils;

import java.io.*;
import java.util.Properties;

public class EnvironmentManager {

    static Properties properties = new Properties();
    public static Properties readPropertiesFile(String env){
       try {
           InputStream input = new FileInputStream("src/test/resources/"+env+".properties");
           properties.load(input);
           System.out.println("URL: "+properties.getProperty("url"));
           System.out.println("Environment: "+properties.getProperty("env"));
       } catch (Exception e) {
           throw new RuntimeException(e);
       }
       return properties;
   }

}
