package com.epam.training.andrea_torres.utils;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotManager implements TestWatcher {

    WebDriver driver;

    public ScreenshotManager(){

    }
    public ScreenshotManager(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        try {
            takeScreenShot();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void takeScreenShot() throws IOException {
        File screenshotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotFile, new File(fileNameGenerator()));
    }

    public String fileNameGenerator() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy-HH-mm-ss");
        String path = "C:\\selenium-results-failed\\";
        String timestamp = formatter.format(new Date());
        return path + "screenshot_" + timestamp + ".png";
    }

}
