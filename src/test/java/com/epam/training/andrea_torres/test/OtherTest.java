package com.epam.training.andrea_torres.test;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class OtherTest {

    @Test
    @Tag("smoke")
    public void test1(){
        System.out.println("Test Run for Tag: Smoke Test");
    }

    @Test
    @Tag("smoke")
    public void test2(){
        System.out.println("Test Run for Tag: Smoke Test");
    }

    @Test
    @Tag("smoke")
    @Tag("regression")
    public void test3(){
        System.out.println("Test Run for Tag: Smoke and Regression Test");
    }

    @Test
    @Tag("regression")
    public void test4(){
        System.out.println("Test Run for Tag: Regression Test");
    }

    @Test
    @Tag("smoke")
    public void test5(){
        System.out.println("Test Run for Tag: Smoke Test");
    }
}
