package com.epam.training.andrea_torres.test;

import com.epam.training.andrea_torres.model.GCComputeEngineForm;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GCHomePageTest extends GCBaseTest {

    @ParameterizedTest
    @MethodSource("provideTestValues")
    @Tag("smoke")
    @Tag("regression")
    public void verifyTotalCostEstimateForComputeEngine(String numberOfInstances, String operativeSystem, String provisionModel, String machineType, String addGPU, String gpuModel, String numberOfGPUs, String localSSD, String region, String committedUsage) throws InterruptedException, IOException {

        String totalEstimatedCost;
       // String totalEstimatedCostModal;

        homePage.navigateHomePage(baseUrl);
        homePage.waitForPage();
        homePage.search("Google Cloud Platform Pricing Calculator");

        searchResults.waitForPage();
        searchResults.selectFirstMatchingResult();

        welcomePricingCalculator.clickOnAddToEstimateButton();

        addToThisEstimateModal.waitForModal();
        addToThisEstimateModal.selectComputeEngine();

        computeEngineForm.waitForPage();

        GCComputeEngineForm formData = new GCComputeEngineForm(numberOfInstances, operativeSystem, provisionModel, machineType, addGPU, gpuModel, numberOfGPUs, localSSD, region, committedUsage);
        computeEngineForm.fillOutComputeEngineForm(formData);

        totalEstimatedCost = computeEngineForm.getTotalEstimatedCost();
        double numericCost = computeEngineForm.convertTotalEstimatedCostToNumber(totalEstimatedCost);
        assertTrue(numericCost > 138.70, "The number is not greater than the default calculation");

        computeEngineForm.openShareEstimateModal();

        shareEstimateModal.waitForModal();
        //totalEstimatedCostModal = shareEstimateModal.getCostEstimate();

        shareEstimateModal.openEstimateSummary();
        costEstimateSummaryPage.waitForPage();

        assertEquals(numberOfInstances,costEstimateSummaryPage.getNumberOfInstances(), "The number of instances are not matching");
        assertEquals(operativeSystem, costEstimateSummaryPage.getOperativeSystem(), " The operative system is not matching");
        assertEquals(provisionModel,costEstimateSummaryPage.getProvisionModel(), "The provision model is not matching");
        assertEquals("n1-standard-8, vCPUs: 8, RAM: 30 GB",costEstimateSummaryPage.getMachineType(),"The machine type is not matching");
        assertEquals(addGPU,costEstimateSummaryPage.getAddGPU(),"The add GPU is not true");
        assertEquals("NVIDIA V100", costEstimateSummaryPage.getGPUModel(), "The GPU model is not matching");
        assertEquals(numberOfGPUs,costEstimateSummaryPage.getNumberOfGPUs(), "The number of GPUs are not matching");
        assertEquals(localSSD, costEstimateSummaryPage.getLocalSSD(), "The local SSD is not matching");
        assertEquals("Netherlands (europe-west4)",costEstimateSummaryPage.getRegion(), "The region is not matching");
        assertEquals(committedUsage,costEstimateSummaryPage.getCommittedUsage(), "The committed usage is not matching");
    }

    private static Stream<Arguments> provideTestValues() {
        return Stream.of(
                Arguments.of(
                        "4","Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)",
                        "Regular","n1-standard-8","true","nvidia-tesla-v100","1", "2x375 GB", "europe-west4", "1 year")
        );
    }

}