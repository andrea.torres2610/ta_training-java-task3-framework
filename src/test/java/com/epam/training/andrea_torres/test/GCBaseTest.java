package com.epam.training.andrea_torres.test;

import com.epam.training.andrea_torres.page.*;
import com.epam.training.andrea_torres.utils.DriverManager;
import com.epam.training.andrea_torres.utils.EnvironmentManager;
import com.epam.training.andrea_torres.utils.ScreenshotManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Properties;

public class GCBaseTest {

    protected WebDriver driver;
    protected WebDriverWait wait;
    protected ScreenshotManager screenshotManager;
    protected GCHomePage homePage;
    protected GCSearchResultsPage searchResults;
    protected GCWelcomePricingCalculatorPage welcomePricingCalculator;
    protected GCAddToThisEstimateModal addToThisEstimateModal;
    protected GCComputeEngineFormPage computeEngineForm;
    protected GCShareEstimateModal shareEstimateModal;
    protected GCCostEstimateSummaryPage costEstimateSummaryPage;
    protected Properties properties;
    protected String baseUrl;

    @BeforeEach
    public void setUp(){

        //Terminal Run
        String env = System.getProperty("env");
        properties = EnvironmentManager.readPropertiesFile(env);
        baseUrl = properties.getProperty("url");

        String browser = System.getProperty("browser");
        driver = DriverManager.createDriver(browser);

        //Run
        /*properties = EnvironmentManager.readPropertiesFile("qa");
        baseUrl = properties.getProperty("url");
        */

        driver.manage().window().maximize();

        //screenshotManager
        screenshotManager = new ScreenshotManager(driver);

        wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        homePage = new GCHomePage(driver);
        searchResults = new GCSearchResultsPage(driver);
        welcomePricingCalculator = new GCWelcomePricingCalculatorPage(driver);
        addToThisEstimateModal = new GCAddToThisEstimateModal(driver);
        computeEngineForm = new GCComputeEngineFormPage(driver);
        shareEstimateModal = new GCShareEstimateModal(driver);
        costEstimateSummaryPage = new GCCostEstimateSummaryPage(driver);
    }

    @AfterEach
    void tearDown() /*throws IOException */{
        //screenshotManager.takeScreenShot();
        driver.quit();
    }

    /*public void waitForPage(WebElement element){
       wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void clickOn(WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public void waitForElement(WebElement element){
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }*/


}
