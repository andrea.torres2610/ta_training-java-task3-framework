package com.epam.training.andrea_torres.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Set;

public class GCShareEstimateModal {

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//div[@class='ZgevAb']")
    private WebElement elementOnPage;
    @FindBy(xpath = "//div[@class='fbc2ib']/label[@class='gN5n4 MyvX5d D0aEmf']")
    private WebElement costEstimate;

    @FindBy(xpath = "//a[@class='tltOzc MExMre rP2xkc jl2ntd']")
    private WebElement openEstimateSummaryLink;

    public GCShareEstimateModal(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public void waitForModal(){
      wait.until(ExpectedConditions.visibilityOf(elementOnPage));
    }

    public String getCostEstimate(){
        return costEstimate.getText();
    }

    public void openEstimateSummary(){
        openEstimateSummaryLink.click();

        String originalWindow = driver.getWindowHandle();

        Set<String> allWindows = driver.getWindowHandles();

        for (String windowHandle : allWindows) {
            if (!windowHandle.equals(originalWindow)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }
    }

}
