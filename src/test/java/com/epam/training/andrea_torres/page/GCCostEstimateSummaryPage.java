package com.epam.training.andrea_torres.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GCCostEstimateSummaryPage {

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//div[@class='NTciLe']/h4[contains(text(),'Cost Estimate Summary')]")
    private WebElement elementOnPage;

    @FindBy(xpath = "//h6[@class='AJUMrc ZF0dQe D0aEmf' and text()='Total estimated cost']")
    private WebElement elementOnPage2;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=7]//span[@class='Kfvdz']")
    private WebElement numberOfInstances;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=8]//span[@class='Kfvdz']")
    private WebElement operativeSystem;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=9]//span[@class='Kfvdz']")
    private WebElement provisionModel;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=3]//span[@class='FDSAhb'][2]//span[@class='Kfvdz']")
    private WebElement machineType;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=13]//span[@class='Kfvdz']")
    private WebElement addGPUs;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=4]//span[@class='FDSAhb'][2]//span[@class='Kfvdz']")
    private WebElement gpuModel;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=4]//span[@class='FDSAhb'][3]//span[@class='Kfvdz']")
    private WebElement numberOfGPUs;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=5]//span[@class='Kfvdz']")
    private WebElement localSSD;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=15]//span[@class='Kfvdz']")
    private WebElement region;

    @FindBy(xpath = "//div[@class='wZCOB']//div[position()=16]//span[@class='Kfvdz']")
    private WebElement commmittedUsage;

    public GCCostEstimateSummaryPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public String getCurrentURL(){
        return driver.getCurrentUrl();
    }

    public void waitForPage(){
       wait.until(ExpectedConditions.visibilityOf(elementOnPage2));
    }

    public String getNumberOfInstances(){
        return numberOfInstances.getText();
    }

    public String getOperativeSystem(){
        return operativeSystem.getText();
    }

    public String getProvisionModel(){
        return provisionModel.getText();
    }

    public String getMachineType(){
        return machineType.getText();
    }

    public String getAddGPU(){
        return addGPUs.getText();
    }

    public String getGPUModel(){
        return gpuModel.getText();
    }

    public String getNumberOfGPUs(){
        return  numberOfGPUs.getText();
    }

    public String getLocalSSD(){
        return localSSD.getText();
    }

    public String getRegion(){
        return region.getText();
    }

    public String getCommittedUsage(){
        return commmittedUsage.getText();
    }

}
