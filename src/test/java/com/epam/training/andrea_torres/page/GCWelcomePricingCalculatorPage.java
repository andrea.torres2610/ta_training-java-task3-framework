package com.epam.training.andrea_torres.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GCWelcomePricingCalculatorPage {

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//button[contains(span[@class='UywwFc-vQzf8d'], 'Add to estimate')]")
    private WebElement addToEstimateButton;

    public GCWelcomePricingCalculatorPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public void waitForPage(){
       wait.until(ExpectedConditions.visibilityOf(addToEstimateButton));

    }

    public void clickOnAddToEstimateButton(){
        wait.until(ExpectedConditions.elementToBeClickable(addToEstimateButton));
        addToEstimateButton.click();
    }

}
