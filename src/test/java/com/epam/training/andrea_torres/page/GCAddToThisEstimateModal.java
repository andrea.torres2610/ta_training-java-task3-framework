package com.epam.training.andrea_torres.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GCAddToThisEstimateModal {

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//*[@class=\"JREjNb\"]")
    private WebElement elementOnPage;
    @FindBy(xpath = "//*[@class=\"JREjNb\"]")
    private WebElement addToThisEstimateModal;

    @FindBy(xpath = "//h2[@class='honxjf' and text()='Compute Engine']")
    private WebElement computeEngine;

    public GCAddToThisEstimateModal(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public void waitForModal(){
        wait.until(ExpectedConditions.visibilityOf(elementOnPage));

    }

    public void selectComputeEngine(){
        computeEngine.click();
    }



}
