package com.epam.training.andrea_torres.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GCSearchResultsPage {

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//div[@class=\"devsite-article-body\"]")
    private WebElement elementOnPage;

    @FindBy(xpath = "//b[contains(text(), 'Google Cloud Pricing Calculator')]")
    private WebElement GCPricingCalculatorText;

    public GCSearchResultsPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public void waitForPage(){
       wait.until(ExpectedConditions.visibilityOf(elementOnPage));
    }

    public void selectFirstMatchingResult(){
        GCPricingCalculatorText.click();
    }
}
