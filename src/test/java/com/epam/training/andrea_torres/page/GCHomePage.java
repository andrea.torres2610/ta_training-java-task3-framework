package com.epam.training.andrea_torres.page;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GCHomePage{

    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//*[@id=\"yDmH0d\"]/c-wiz[1]/div/div/div/section[1]/div")
    private WebElement elementOnPage;
    @FindBy(xpath = "//input[@class='mb2a7b']")
    private WebElement search;

    public GCHomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public void navigateHomePage(String baseUrl){
        driver.get(baseUrl);
    }

    public void waitForPage(){
       wait.until(ExpectedConditions.visibilityOf(elementOnPage));
    }

    public void search(String text){

        search.click();
        search.sendKeys(text);
        search.sendKeys(Keys.ENTER);

    }

}