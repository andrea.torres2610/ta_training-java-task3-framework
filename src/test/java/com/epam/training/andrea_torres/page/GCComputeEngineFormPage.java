package com.epam.training.andrea_torres.page;

import com.epam.training.andrea_torres.model.GCComputeEngineForm;
import com.epam.training.andrea_torres.test.GCBaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class GCComputeEngineFormPage extends GCBaseTest {
    WebDriver driver;
    WebDriverWait wait;

    @FindBy(xpath = "//div[@class=\"vHartc\"]")
    private WebElement elementOnPage;

    @FindBy(xpath = "//input[@id='c11']")
    private WebElement numberOfInstances;

    @FindBy(xpath = "//div[@aria-labelledby='c31 c33']")
    private WebElement machineTypeDropdown;

    @FindBy(xpath = "//li[@data-value='n1-standard-8']")
    private WebElement machineTypeOptionStandard8;

    @FindBy(xpath = "//button[@aria-label='Add GPUs']")
    private WebElement addGPUsToggleButton;

    @FindBy(xpath = "//div[@class='U4lDT']//div[position()=23]//div[@class='VfPpkd-TkwUic']")
    private WebElement gpuModelDropdown;

    @FindBy(xpath = "//li[@data-value='nvidia-tesla-v100']")
    private WebElement gpuModelOptionNVDIATeslaV100option;

    @FindBy(xpath = "//div[@class='U4lDT']//div[position()=27]//div[@class='VfPpkd-TkwUic']")
    private WebElement localSSDropdown;

    @FindBy(xpath = "//li[@data-value='2' and .//span[text()='2x375 GB']]")
    private WebElement localSSDOption2x375GB;

    @FindBy(xpath = "//div[@class='U4lDT']//div[position()=29]//div[@class='VfPpkd-TkwUic']")
    private WebElement regionDropdown;

    @FindBy(xpath = "//li[@data-value='europe-west4']")
    private WebElement regionOptionEuropewest4;

    @FindBy(xpath = "//label[@class='zT2df' and @for='1-year']")
    private WebElement committed1YearButton;

    @FindBy(xpath = "//button[@aria-label='Open Share Estimate dialog']")
    private WebElement shareButton;

    @FindBy(xpath = "//div[contains(@class, 'fbc2ib')]//label[@class='gt0C8e MyvX5d D0aEmf']")
    private WebElement totalEstimatedCost;

    public GCComputeEngineFormPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    public void waitForPage(){
        wait.until(ExpectedConditions.visibilityOf(elementOnPage));
    }

    public void scrollDown(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0, 500);");
    }

    public void scrollUp(){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, 0);");
    }

    public void setNumberOfInstances(String numberOfInstances){
        this.numberOfInstances.clear();
        this.numberOfInstances.sendKeys(numberOfInstances);
    }

    public void setMachineType(String machineType){
        machineTypeDropdown.click();
        WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@data-value='"+machineType+"']")));
        option.click();
    }

    public void setGPU(String gpuModel){
        wait.until(ExpectedConditions.elementToBeClickable(gpuModelDropdown));
        gpuModelDropdown.click();

        WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@data-value='"+gpuModel+"']")));
        option.click();
    }

    public void setLocalSSD(String localSSD){
        localSSDropdown.click();
        WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@data-value='2' and .//span[text()='"+localSSD+"']]")));
        option.click();
    }

    public void setRegion(String region){
        regionDropdown.click();
        WebElement option = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@data-value='"+region+"']")));
        option.click();
    }

    public void fillOutComputeEngineForm(GCComputeEngineForm formData){

        setNumberOfInstances(formData.getNumberOfInstances());
        setMachineType(formData.getMachineType());

        addGPUsToggleButton.click();

        scrollDown();

        setGPU(formData.getGpuModel());
        setLocalSSD(formData.getLocalSSD());
        setRegion(formData.getRegion());

        scrollDown();

        committed1YearButton.click();

    }

    public String getTotalEstimatedCost() throws InterruptedException {
        Thread.sleep(2000);
        return totalEstimatedCost.getText();
    }

    public void openShareEstimateModal() throws InterruptedException {
        shareButton.click();
    }

    public double convertTotalEstimatedCostToNumber(String totalEstimatedCost){
        String numericCost = totalEstimatedCost.replace("$", "").replace(",", "");
        double parsedCost = Double.parseDouble(numericCost);
        return parsedCost;
    }


}
