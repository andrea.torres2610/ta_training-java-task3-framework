package com.epam.training.andrea_torres.model;

public class GCComputeEngineForm {

    private String numberOfInstances;
    private String operativeSystem;
    private String provisionModel;
    private String machineType;
    private String addGPU;
    private String gpuModel;
    private String numberOfGPUs;
    private String localSSD;
    private String region;
    private String committedUsage;


    public GCComputeEngineForm(String numberOfInstances, String operativeSystem, String provisionModel, String machineType, String addGPU, String gpuModel, String numberOfGPUs, String localSSD, String region, String committedUsage) {
        this.numberOfInstances = numberOfInstances;
        this.operativeSystem = operativeSystem;
        this.provisionModel = provisionModel;
        this.machineType = machineType;
        this.addGPU = addGPU;
        this.gpuModel = gpuModel;
        this.numberOfGPUs = numberOfGPUs;
        this.localSSD = localSSD;
        this.region = region;
        this.committedUsage = committedUsage;
    }

    public String getNumberOfInstances() {
        return numberOfInstances;
    }

    public void setNumberOfInstances(String numberOfInstances) {
        this.numberOfInstances = numberOfInstances;
    }

    public String getOperativeSystem() {
        return operativeSystem;
    }

    public void setOperativeSystem(String operativeSystem) {
        this.operativeSystem = operativeSystem;
    }

    public String getProvisionModel() {
        return provisionModel;
    }

    public void setProvisionModel(String provisionModel) {
        this.provisionModel = provisionModel;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public String getAddGPU() {
        return addGPU;
    }

    public void setAddGPU(String addGPU) {
        this.addGPU = addGPU;
    }

    public String getGpuModel() {
        return gpuModel;
    }

    public void setGpuModel(String gpuModel) {
        this.gpuModel = gpuModel;
    }

    public String getNumberOfGPUs() {
        return numberOfGPUs;
    }

    public void setNumberOfGPUs(String numberOfGPUs) {
        this.numberOfGPUs = numberOfGPUs;
    }

    public String getLocalSSD() {
        return localSSD;
    }

    public void setLocalSSD(String localSSD) {
        this.localSSD = localSSD;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCommittedUsage() {
        return committedUsage;
    }

    public void setCommittedUsage(String committedUsage) {
        this.committedUsage = committedUsage;
    }

}
